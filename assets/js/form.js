const form = document.getElementById('add_member');
const formElementsToValidate = form.querySelectorAll('[required]');

const getFormElements = (inputNames = []) => {
  const formValues = {};

  inputNames.forEach((name) => {
    const formElement = form.querySelector(`[name="${name}"]`);
    formValues[name] = null;

    if (formElement && formElement.value) {
      formValues[name] = formElement.value.toString() || null;
    }
  });

  return formValues;
};

const getBadgeValue = () => {
  const badges = form.querySelector('[name="badges"]');
  const selectedBadges = [...badges.selectedOptions].map(
    (option) => option.value
  );
  return selectedBadges;
};

const getProjectValue = () => {
  const projects = form.querySelector('[name="projects"]');
  const selectedProjects = [...projects.selectedOptions].map(
    (option) => option.value
  );
  return selectedProjects;
};

const getEventValue = () => {
  const events = form.querySelector('[name="events"]');
  const selectedEvents = [...events.selectedOptions].map(
    (option) => option.value
  );
  return selectedEvents;
};

function validateSubmission() {
  const formElements = [...document.querySelectorAll('input')].map(
    (item) => item.name
  );

  const {
    user_id,
    name,
    nick,
    title,
    pronoun,
    country,
    avatar,
    active,
    description,
    gitlab,
    twitter,
    matrix,
    github,
    instagram,
    dev_to,
    mastodon,
    blog,
    keybase,
  } = getFormElements(formElements);

  const badges = getBadgeValue();
  const projects = getProjectValue();
  const events = getEventValue();

  const id = user_id;

  // Generate a filename
  const filename = user_id.replace(/\s/g, '-') + '.yml';

  const yamlEntries = {
    id,
    name,
    nick,
    title,
    pronoun,
    country,
    description,
    avatar,
    badges,
    active,
    projects,
    events,
    social: {
      blog,
      twitter,
      gitlab,
      matrix,
      instagram,
      dev_to,
      github,
      mastodon,
      keybase,
    },
  };

  let yaml = `---`;

  const mapEntries = (amountOfSpaces = 0) => ([key, value]) => {
    const padSpaces = (amount) =>
      amount > 0 ? `  `.repeat(amount) : '';

    if (!value) {
      return;
    }
    // handle the case when the value is an array (eg: badges) or object (eg: social)
    if (typeof value === 'object') {

      // handles when value is an array (eg: badges)
      if (Array.isArray(value) && value.length > 0) {
        yaml += `\n${padSpaces(amountOfSpaces)}${key}:`;

        const mapValuesToYaml = value.map((val) => `\n${padSpaces(amountOfSpaces + 1)}- ${val}`).join('');

        yaml += mapValuesToYaml;

        return;
      }

      const nonEmptyEntries = Object.entries(value).filter(([, val]) => !!val);

      if (nonEmptyEntries.length > 0) {
        yaml += `\n${padSpaces(amountOfSpaces)}${key}:`;

        // handles when value is an object (eg: social)
        nonEmptyEntries.forEach(mapEntries(amountOfSpaces + 1));

        return;
      }

      return;
    }

    yaml += `\n${padSpaces(amountOfSpaces)}${key}: ${value}`;
  };

  Object.entries(yamlEntries).forEach(mapEntries(0));

  yaml += `\n---`

  // Encode string to URI format
  const encodedFileText = encodeURIComponent(yaml);

  // Generate a gitlab link with query parameter
  const gitlabQueryLink =
    'https://gitlab.gnome.org/Teams/Engagement/websites/faces-of-gnome/-/new/main/_data%2Fmembers?file=' +
    encodedFileText +
    '&file_name=' +
    filename;

  // Open in a new tab
  window.open(gitlabQueryLink);
}

form.addEventListener('submit', (event) => {
  event.preventDefault();

  if (form.checkValidity() === false) {
    for (var i = 0; i < formElementsToValidate.length; i++) {
      formElementsToValidate[i].closest('.form-group').classList.add('was-validated');
    }

    return form.scrollIntoView(true);
  }

  return validateSubmission();
});